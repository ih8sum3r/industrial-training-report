\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Introduction to Organization}{1}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Testing and Consutancy Cell}{1}{subsection.1.1.1}
\contentsline {section}{\numberline {1.2}Introduction to Project}{2}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Software requirements}{4}{subsection.1.2.1}
\contentsline {section}{\numberline {1.3}Project Category}{5}{section.1.3}
\contentsline {section}{\numberline {1.4}Objectives}{5}{section.1.4}
\contentsline {section}{\numberline {1.5}Problem Formulation and Reorganization of Need}{5}{section.1.5}
\contentsline {section}{\numberline {1.6}Existing System}{6}{section.1.6}
\contentsline {section}{\numberline {1.7}Proposed System}{7}{section.1.7}
\contentsline {section}{\numberline {1.8}Unique Features of the System}{9}{section.1.8}
\contentsline {chapter}{\numberline {2}Requirement Analysis and System Specification}{10}{chapter.2}
\contentsline {section}{\numberline {2.1}Feasibility study}{10}{section.2.1}
\contentsline {section}{\numberline {2.2}Software requirement specification document}{10}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Function Requirement}{10}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Non Functional Requirements}{10}{subsection.2.2.2}
\contentsline {section}{\numberline {2.3}Software and Hardware Requirement}{11}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Software requirements}{11}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Hardware Requirements}{11}{subsection.2.3.2}
\contentsline {section}{\numberline {2.4}Validation}{11}{section.2.4}
\contentsline {section}{\numberline {2.5}Expected hurdles}{12}{section.2.5}
\contentsline {section}{\numberline {2.6}SDLC model to be used}{13}{section.2.6}
\contentsline {chapter}{\numberline {3}System Design}{14}{chapter.3}
\contentsline {section}{\numberline {3.1}Design Approach}{14}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}SA/SD methodology}{15}{subsection.3.1.1}
\contentsline {section}{\numberline {3.2}Detailed Design}{16}{section.3.2}
\contentsline {section}{\numberline {3.3}System Design using design tools}{17}{section.3.3}
\contentsline {section}{\numberline {3.4}User Interface Design}{18}{section.3.4}
\contentsline {section}{\numberline {3.5}Database Design}{20}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}ER Diagrams}{20}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}Normalization}{21}{subsection.3.5.2}
\contentsline {subsection}{\numberline {3.5.3}Database Manipulation}{22}{subsection.3.5.3}
\contentsline {subsection}{\numberline {3.5.4}Database Connection Controls and Strings}{23}{subsection.3.5.4}
\contentsline {section}{\numberline {3.6}Methodology}{23}{section.3.6}
\contentsline {chapter}{\numberline {4}Implementation, Testing and Maintenance}{25}{chapter.4}
\contentsline {section}{\numberline {4.1}Introduction to Languages}{25}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Inroduction to CodeIgniter}{25}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Introduction to \LaTeX }{25}{subsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.2.1}Typesetting}{26}{subsubsection.4.1.2.1}
\contentsline {subsubsection}{\numberline {4.1.2.2}Installing \LaTeX {} on System}{26}{subsubsection.4.1.2.2}
\contentsline {subsubsection}{\numberline {4.1.2.3}Pdfscreen \LaTeX {}}{27}{subsubsection.4.1.2.3}
\contentsline {subsection}{\numberline {4.1.3}Introduction to Compass}{28}{subsection.4.1.3}
\contentsline {subsubsection}{\numberline {4.1.3.1}Installation of Compass}{29}{subsubsection.4.1.3.1}
\contentsline {subsubsection}{\numberline {4.1.3.2}Creating Project in Compass}{29}{subsubsection.4.1.3.2}
\contentsline {subsubsection}{\numberline {4.1.3.3}Compiling Sass to CSS}{29}{subsubsection.4.1.3.3}
\contentsline {subsubsection}{\numberline {4.1.3.4}Compass Configuration}{29}{subsubsection.4.1.3.4}
\contentsline {subsection}{\numberline {4.1.4}Introduction to MySQL}{29}{subsection.4.1.4}
\contentsline {section}{\numberline {4.2}Implementation Of Project}{30}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Fetching of data from the database}{31}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Typesetting of data}{32}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Creating a DVI file}{32}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Creating a postscript file}{33}{subsection.4.2.4}
\contentsline {subsection}{\numberline {4.2.5}Use In Printing}{33}{subsection.4.2.5}
\contentsline {subsection}{\numberline {4.2.6}Creating a PDF file}{34}{subsection.4.2.6}
\contentsline {subsection}{\numberline {4.2.7}Imposition of the pdf}{35}{subsection.4.2.7}
\contentsline {subsection}{\numberline {4.2.8}Color separation of the pdf}{37}{subsection.4.2.8}
\contentsline {subsection}{\numberline {4.2.9}RGB}{37}{subsection.4.2.9}
\contentsline {subsection}{\numberline {4.2.10}CMYK}{37}{subsection.4.2.10}
\contentsline {subsection}{\numberline {4.2.11}RGB to CMYK}{38}{subsection.4.2.11}
\contentsline {section}{\numberline {4.3}Evaluation and Maintenance}{39}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}RGB To CMYK Example :-}{41}{subsection.4.3.1}
\contentsline {section}{\numberline {4.4}Coding standards of Language used}{41}{section.4.4}
\contentsline {section}{\numberline {4.5}Project Scheduling}{43}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Gantt Charts}{43}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}PERT Chart}{44}{subsection.4.5.2}
\contentsline {section}{\numberline {4.6}Testing Techniques and Test Plans}{45}{section.4.6}
\contentsline {chapter}{\numberline {5}Results and Discussions}{47}{chapter.5}
\contentsline {section}{\numberline {5.1}User Interface Representation}{47}{section.5.1}
\contentsline {section}{\numberline {5.2}Snapshots of system with brief detail of each}{48}{section.5.2}
\contentsline {section}{\numberline {5.3}Back Ends Representation}{50}{section.5.3}
\contentsline {chapter}{\numberline {6}Conclusion and Future Scope}{53}{chapter.6}
\contentsline {section}{\numberline {6.1}Conclusion}{53}{section.6.1}
\contentsline {section}{\numberline {6.2}Current status}{53}{section.6.2}
\contentsline {section}{\numberline {6.3}Scope}{53}{section.6.3}
